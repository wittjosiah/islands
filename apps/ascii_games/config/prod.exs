# Since configuration is shared in umbrella projects, this file
# should only configure the :ascii_games application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

config :slack,
  api_token: "${SLACK_TOKEN}",
  bot_token: "${SLACK_BOT_TOKEN}"
