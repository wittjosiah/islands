defmodule AsciiGames.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      %{
        id: Slack.Bot,
        start:
          {Slack.Bot, :start_link,
           [
             AsciiGames.SlackRtm,
             [],
             Application.get_env(:slack, :bot_token),
             %{name: :slack_bot}
           ]}
      }
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: AsciiGames.Supervisor)
  end
end
