defmodule AsciiGames.Games do
  @moduledoc """
  TODO
  """

  @doc """
  TODO
  """
  def get_action(input, player) do
    case input |> String.trim() |> String.split(" ") do
      ["start", game_type, opponent] ->
        {:ok,
         %{
           action: :start,
           game_type: String.to_existing_atom(game_type),
           game_id: game_id(player, opponent),
           player: player,
           opponent: opponent
         }}

      ["accept", game_type, opponent] ->
        game_type = String.to_existing_atom(game_type)

        with {:ok, game} <- get_game(game_type, player, opponent),
             do:
               {:ok,
                %{
                  action: :accept,
                  game_type: game_type,
                  game: game,
                  player: player,
                  opponent: opponent
                }}

      ["position", "island", opponent, island, row, col] ->
        with {:ok, game} <- get_game(:islands, player, opponent),
             do:
               {:ok,
                %{
                  action: :position_island,
                  game_type: :islands,
                  game: game,
                  player: player,
                  opponent: opponent,
                  island: String.to_existing_atom(island),
                  row: String.to_integer(row),
                  col: String.to_integer(col)
                }}

      ["set", "islands", opponent] ->
        with {:ok, game} <- get_game(:islands, player, opponent),
             do:
               {:ok,
                %{
                  action: :set_islands,
                  game_type: :islands,
                  game: game,
                  player: player,
                  opponent: opponent
                }}

      ["guess", "coordinate", opponent, row, col] ->
        with {:ok, game} <- get_game(:islands, player, opponent),
             do:
               {:ok,
                %{
                  action: :guess_coordinate,
                  game_type: :islands,
                  game: game,
                  player: player,
                  opponent: opponent,
                  row: String.to_integer(row),
                  col: String.to_integer(col)
                }}

      ["quit", game_type, opponent] ->
        {:ok,
         %{
           action: :quit,
           game_type: String.to_existing_atom(game_type),
           game_id: game_id(player, opponent),
           player: player,
           opponent: opponent
         }}

      ["show", game_type, opponent] ->
        game_type = String.to_existing_atom(game_type)

        with {:ok, game} <- get_game(:islands, player, opponent),
             do:
               {:ok,
                %{
                  action: :show,
                  game_type: game_type,
                  game: game,
                  player: player,
                  opponent: opponent
                }}

      _ ->
        {:error, :unknown_action}
    end
  end

  defp game_id(player_a, player_b),
    do:
      [player_a, player_b]
      |> Enum.map(&unescape_player/1)
      |> Enum.sort()
      |> Enum.join(":")

  defp unescape_player("<@" <> player), do: String.slice(player, 0, String.length(player) - 1)
  defp unescape_player(player), do: player

  defp get_game(:islands, player, opponent),
    do: {:ok, player |> game_id(opponent) |> IslandsEngine.Game.via_tuple()}

  defp get_game(_, _, _), do: {:error, :unknown_game_type}

  @doc """
  TODO
  """
  def perform_action(%{action: :start, game_type: :islands, game_id: game_id, player: player}) do
    case IslandsEngine.GameSupervisor.start_game(game_id, player) do
      {:ok, _} -> :ok
      {:error, {:already_started, _}} -> {:error, :already_started}
      _ -> :error
    end
  end

  def perform_action(%{action: :start}), do: {:error, :unknown_game_type}

  def perform_action(%{action: :accept, game_type: :islands, game: game, player: player}),
    do: IslandsEngine.Game.add_player(game, player)

  def perform_action(%{action: :accept}), do: {:error, :unknown_game_type}

  def perform_action(%{
        action: :position_island,
        game: game,
        player: player,
        island: island,
        row: row,
        col: col
      }),
      do: IslandsEngine.Game.position_island(game, player, island, row, col)

  def perform_action(%{action: :set_islands, game: game, player: player}),
    do: IslandsEngine.Game.set_islands(game, player)

  def perform_action(%{
        action: :guess_coordinate,
        game: game,
        player: player,
        row: row,
        col: col
      }),
      do: IslandsEngine.Game.guess_coordinate(game, player, row, col)

  def perform_action(%{action: :quit, game_type: :islands, game_id: game_id}),
    do: IslandsEngine.GameSupervisor.stop_game(game_id)

  def perform_action(%{action: :quit}), do: {:error, :unknown_game_type}

  def perform_action(%{action: :show, game_type: :islands}), do: :ok

  def perform_action(%{action: :show}), do: {:error, :unknown_game_type}

  def perform_action(_), do: {:error, :unknown_action}

  @doc """
  TODO
  """
  def prepare_response({:error, :unknown_action}), do: "Unknown action"
  def prepare_response({:error, :unknown_game_type}), do: "Unknown game type"
  def prepare_response({:error, :already_started}), do: "Already started"
  def prepare_response({:error, :invalid_coordinate}), do: "Invalid coordinate"
  def prepare_response({:error, :invalid_island_type}), do: "Invalid island type"
  def prepare_response({:error, :simple_one_for_one}), do: "No game to quit"
  def prepare_response(:error), do: "Unexpected error"

  @doc """
  TODO
  """
  def prepare_responses(:ok, %{action: :quit, player: player}),
    do: {"Game ended", "#{player} ended game"}

  def prepare_responses(:ok, %{action: :start, game_type: game_type, player: player}),
    do: {"Game started", "#{player} invited you to play #{game_type}"}

  def prepare_responses(:ok, %{
        action: :show,
        game_type: :islands,
        game: game,
        player: player
      }) do
    game_text = IslandsEngine.Game.game_to_ascii(game, player)
    {"```#{game_text}```", nil}
  end

  def prepare_responses(:ok, %{game: game, player: player, opponent: opponent}) do
    player_game_text = IslandsEngine.Game.game_to_ascii(game, player)
    opponent_game_text = IslandsEngine.Game.game_to_ascii(game, opponent)
    {"Move successful\n```#{player_game_text}```", "#{player} moved\n```#{opponent_game_text}```"}
  end
end
