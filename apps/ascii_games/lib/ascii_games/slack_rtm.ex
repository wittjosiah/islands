defmodule AsciiGames.SlackRtm do
  use Slack

  require Logger
  alias AsciiGames.Games
  alias Slack.Web.{Chat, Im}

  def handle_connect(slack, state) do
    Logger.info("Connected as #{slack.me.name} in team #{slack.team.domain}")
    {:ok, state}
  end

  def handle_event(message = %{type: "message", user: player}, slack, state) do
    Logger.info("Received message: #{inspect(message)}")

    with {:ok, action} <- Games.get_action(message.text, player),
         :ok <- Games.perform_action(action),
         {player_msg, opponent_msg} <- Games.prepare_responses(:ok, action) do
      if opponent_msg != nil do
        opponent =
          action.opponent |> String.replace_prefix("<@", "") |> String.replace_suffix(">", "")

        %{"channel" => %{"id" => opponent_channel}} = Im.open(opponent)
        Chat.post_message(opponent_channel, opponent_msg)
      end

      send_message(player_msg, message.channel, slack)
    else
      error -> send_message(Games.prepare_response(error), message.channel, slack)
    end

    {:ok, state}
  end

  def handle_event(_, _, state), do: {:ok, state}

  def handle_info({:message, text, channel}, slack, state) do
    send_message(text, channel, slack)
    {:ok, state}
  end

  def handle_info(_, _, state), do: {:ok, state}
end
