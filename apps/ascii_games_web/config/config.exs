# Since configuration is shared in umbrella projects, this file
# should only configure the :ascii_games_web application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# General application configuration
config :ascii_games_web,
  generators: [context_app: :ascii_games]

# Configures the endpoint
config :ascii_games_web, AsciiGamesWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "IYeAAdlZbhqUw4vJmVDT3d9EnMC+ObeZEycn5oL+CApw98aAy+j1OQ7860A63as2",
  render_errors: [view: AsciiGamesWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: AsciiGamesWeb.PubSub, adapter: Phoenix.PubSub.PG2]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
