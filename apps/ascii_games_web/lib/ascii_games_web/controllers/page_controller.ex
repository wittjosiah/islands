defmodule AsciiGamesWeb.PageController do
  use AsciiGamesWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
