defmodule IslandsEngine.Board do
  alias IslandsEngine.{Coordinate, Island}

  def new(), do: %{missed_coordinates: MapSet.new(), islands: %{}}

  def position_island(%{islands: islands} = board, key, %Island{} = island) do
    case overlaps_existing_island?(islands, key, island) do
      true -> {:error, :overlapping_island}
      false -> %{board | islands: Map.put(islands, key, island)}
    end
  end

  defp overlaps_existing_island?(islands, new_key, new_island) do
    Enum.any?(islands, fn {key, island} ->
      key != new_key and Island.overlaps?(island, new_island)
    end)
  end

  def all_islands_positioned?(islands), do: Enum.all?(Island.types(), &Map.has_key?(islands, &1))

  def guess(board, %Coordinate{} = coordinate) do
    board
    |> check_all_islands(coordinate)
    |> guess_response(board, coordinate)
  end

  defp check_all_islands(%{islands: islands}, coordinate) do
    Enum.find_value(islands, :miss, fn {key, island} ->
      case Island.guess(island, coordinate) do
        {:hit, island} -> {key, island}
        :miss -> false
      end
    end)
  end

  defp guess_response({key, island}, %{islands: islands} = board, _) do
    islands = %{islands | key => island}
    board = %{board | islands: islands}
    {:hit, forest_check(board, key), win_check(board), board}
  end

  defp guess_response(:miss, %{missed_coordinates: missed} = board, coordinate) do
    board = %{board | missed_coordinates: MapSet.put(missed, coordinate)}
    {:miss, :none, :no_win, board}
  end

  defp forest_check(%{islands: islands}, key) do
    case forested?(islands, key) do
      true -> key
      false -> :none
    end
  end

  defp forested?(islands, key) do
    islands
    |> Map.fetch!(key)
    |> Island.forested?()
  end

  defp win_check(board) do
    case all_forested?(board) do
      true -> :win
      false -> :no_win
    end
  end

  defp all_forested?(%{islands: islands}),
    do: Enum.all?(islands, fn {_key, island} -> Island.forested?(island) end)

  def to_string(%{islands: islands, missed_coordinates: misses}, show_islands \\ false) do
    hits =
      Enum.reduce(islands, MapSet.new(), fn {_key, island}, acc ->
        MapSet.union(acc, island.hit_coordinates)
      end)

    islands =
      if show_islands do
        Enum.reduce(islands, MapSet.new(), fn {_key, island}, acc ->
          MapSet.union(acc, island.coordinates)
        end)
      else
        MapSet.new()
      end

    string_helper(islands, hits, misses)
  end

  defp string_helper(
         islands,
         hits,
         misses,
         row \\ 0,
         column \\ 0,
         output \\ "  0 1 2 3 4 5 6 7 8 9 \n"
       )

  defp string_helper(_islands, _hits, _misses, 10, 0, output), do: output

  defp string_helper(islands, hits, misses, row, column, output) do
    {:ok, coordinate} = Coordinate.new(row, column)

    symbol =
      cond do
        MapSet.member?(hits, coordinate) -> "x"
        MapSet.member?(misses, coordinate) -> "m"
        MapSet.member?(islands, coordinate) -> "o"
        true -> " "
      end

    output =
      case {row, column} do
        {_, 0} -> output <> "#{row}|#{symbol}"
        {_, 9} -> output <> "|#{symbol}|\n"
        _ -> output <> "|#{symbol}"
      end

    {next_row, next_column} =
      case {row, column} do
        {_, 9} -> {row + 1, 0}
        _ -> {row, column + 1}
      end

    string_helper(islands, hits, misses, next_row, next_column, output)
  end
end
